public class Rectangle {
    private float length; //uzunluq
    private float width;  //en

    public Rectangle(float length, float width){
        this.length=length;
        this.width= width;
    }
    public Rectangle() {
    }

    public float getLength() {
        return length;
    }
    public float getWidth(){
        return width;
    }

    public float getPerimetr(){
        return width*2+ length*2;
    }
    public float getArea(){
        return width*length;
    }
}
