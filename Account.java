import javax.swing.plaf.synth.SynthOptionPaneUI;

public class Account {
    private String id;
    private String name;
    private int balance;

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int credit(int amount) {
        return balance + amount;
    }

    public int debit(int amount) {
        if (amount <= balance) {
            return balance - amount;
        }else {
            System.out.println("Amount exceeded balance");
        }
        return balance;
    }
    public void transferTo(Account destinationAccount, double amount) {
        if (amount > 0 && this.balance >= amount) {
            this.balance -= amount;
            destinationAccount.balance += amount;
        } else {
            System.out.println("Transfer failed: Insufficient funds");
        }
    }

}
