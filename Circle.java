public class Circle {
    private double radius1;
    private double radius2;
    private String color;

    public Circle() {
    }

    public Circle(double radius1, double radius2, String color) {
        this.radius1 = radius1;
        this.radius2 = radius2;
        this.color = color;
    }

    public Circle(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getRadius1() {
        return radius1;
    }
    public  double getRadius2(){
        return radius2;
    }
    public double getArea() {
        return radius1*radius2*Math.PI;
    }
    public String getColor(){
        return color;
    }
}
